FROM archlinux/base:latest

ADD files/ /

# Update
RUN pacman --noconfirm -Syu

# Install PHP
RUN pacman --noconfirm --needed -S php71 php71-gd php71-intl php71-mcrypt

RUN ln -s /usr/bin/php71 /usr/bin/php

# Clean up
RUN rm -f \
      /var/cache/pacman/pkg/* \
      /var/lib/pacman/sync/* \
      /etc/pacman.d/mirrorlist.pacnew

